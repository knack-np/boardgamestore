<?php

use Illuminate\Database\Seeder;

class GameGenresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('game_genres')->insert([
            ['game_id' => '1','genre_id' => '1'],
            ['game_id' => '1','genre_id' => '4'],
            ['game_id' => '1','genre_id' => '3']
        ]);
    }
}
