<?php

use Illuminate\Database\Seeder;

class GenresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('genres')->insert([
            ['name_th' => 'เด็ก','name_eng' => 'kid'],
            ['name_th' => 'ครอบครัว','name_eng' => 'family'],
            ['name_th' => 'กลยุทธ์','name_eng' => 'strategy'],
            ['name_th' => 'ปาร์ตี้' ,'name_eng' => 'party']
        ]);
    }
}
