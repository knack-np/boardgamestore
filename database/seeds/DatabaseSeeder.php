<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            GenresTableSeeder::class,
            LanguagesTableSeeder::class,
            GamesTableSeeder::class,
            GameGenresTableSeeder::class,
        ]);
        // $this->call(GamesTableSeeder::class);
    }
}
