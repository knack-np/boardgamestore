<?php

use Illuminate\Database\Seeder;

class GamesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('games')->insert(
            [
            'name_th' => 'Terra Mystica',
            'name_eng' => 'Terra Mystica',
            'complexity_level' => '3',
            'min_player' => '2',
            'max_player' => '4',
            'age_to_play' => '14',
            'time_to_play' => '60',
            'language_id' => '1',
            'description_th' => 'เกมนี้ผู้เล่นจะได้รับบทเป็นแต่ละเผ่า โดยที่จะต้องสร้างความเจริญให้กับเผ่าให้ได้มากที่สุดจากแต้มชัยชนะ
            ฟังดูเหมือนง่าย แต่เกมนี้มีวิธีเล่นที่ยืดยาวที่บางคนอาจจะหนีเมื่อได้ฟัง แต่ถ้าได้ลองเล่นแล้วจะพบว่ามันก็ไม่ซับซ้อนเหมือนที่คิด แล้วติดใจแบบไม่รู้ตัว
            จุดเด่นของเกมส์นี้คือมีเผ่าให้เลือกมากมายถึง 14เผ่าซึ่งมีความสามารถต่างกัน ทำให้เล่นได้หลายรอบไม่มีเบื่อ ข้อดีอีกอย่างของเกมส์นี้คือ ไม่ต้องใช้ภาษา เพราะทุกอย่างแปลงเป็นสัญลักษณ์หมดแล้ว
            รวมไปถึง component ที่ทำออกมาได้ดี จึงทำให้น้ำหนักมาก และราคาแอบแพง เกมส์นี้มีดีอย่างไรถึงไต่อันดับ topten ได้อย่างรวดเร็ว รวมถึง out of printแม้ราคาจะแพงก็ตาม' ,

            'description_eng' => 'In Terra Mystica, each player chooses one of the 14 different factions and must use their abilities to develop the
            land more successfully then their opponents. Each faction is bound to a specific type of terrain and can only build on that terrain type.
            Players will use the several different currencies in the game to both upgrade their civilization' ,
            'author' => 'knack',
            'designer' => 'knack',
            'publisher' => 'knack',
            'price' => '5800',
            ]
        );
    }
}
