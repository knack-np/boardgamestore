<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('games', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name_th');
            $table->string('name_eng')->nullable();
            $table->unsignedInteger('language_id');
            $table->tinyInteger('complexity_level')->nullable();
            $table->integer('min_player');
            $table->integer('max_player');
            $table->integer('age_to_play')->nullable();
            $table->integer('time_to_play');
            $table->longText('description_th');
            $table->longText('description_eng')->nullable();
            $table->string('author')->nullable();
            $table->string('designer')->nullable();
            $table->string('publisher')->nullable();
            $table->float('price', 8, 2);
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('games', function (Blueprint $table) {
            $table->foreign('language_id')->references('id')->on('languages');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('games');
    }
}
