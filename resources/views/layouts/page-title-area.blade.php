<div class="page-title-area">
    <div class="row align-items-center">
        <div class="col-sm-6">
            <div class="breadcrumbs-area clearfix">
                <h4 class="page-title pull-left"> {{$page_name}} </h4>
                @isset($action)
                <ul class="breadcrumbs pull-left">
                    <li><a href="/{{strtolower($page_name)}}">index</a></li>
                    <li><span>{{$action}}</span></li>
                </ul>
                @endisset
            </div>
        </div>
        <div class="col-sm-6 clearfix">
            <div class="user-profile pull-right">
                <img class="avatar user-thumb" src="{{asset('images/author/avatar.png')}}" alt="avatar">
                {{-- <h4 class="user-name dropdown-toggle" data-toggle="dropdown">BoardGame</h4> --}}
            </div>
        </div>
    </div>
</div>
