<div class="sidebar-menu">
    <div class="sidebar-header">
        <div class="logo">
            <a href="index.html"><img src="{{asset('images/icon/logo.png')}}" alt="logo"></a>
        </div>
    </div>
    <div class="main-menu">
        <div class="menu-inner">
            <nav>
                <ul class="metismenu" id="menu">
                    <li class="active">
                        <a href="javascript:void(0)" aria-expanded="true"><i class="fa fa-table"></i>
                                    <span>Tables</span></a>
                        <ul class="collapse">
                            <li @if ($page_name=="Game" ) class="active" @endif><a href="/game">Game</a></li>
                            <li @if ($page_name=="Genre" ) class="active" @endif><a href="/genre">Genre</a></li>
                            <li @if ($page_name=="Language" ) class="active" @endif><a href="/language">Language</a></li>
                        </ul>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</div>
