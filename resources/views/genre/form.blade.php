<div class="form-group">
    <label for="name_th" class="col-form-label">Name Thai</label>
    <input class="form-control" type="text" placeholder="ปาร์ตี้" id="name_th" name="name_th" @isset($genre) value="{{$genre->name_th}}" @endisset>
</div>
<div class="form-group">
    <label for="name_eng" class="col-form-label">Name Eng</label>
    <input class="form-control" type="search" placeholder="Party" id="name_eng" name="name_eng" @isset($genre) value="{{$genre->name_eng}}" @endisset>
</div>

