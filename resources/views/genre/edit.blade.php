@extends('app')
@section('content')
<div class="main-content-inner">
    <div class="row">
        <!-- Textual inputs start -->
        <div class="col-12 mt-5">
            <div class="card">
                <div class="card-body">
                    <form action="/genre/{{$genre->id}}" method="POST">
                        @method('PUT')
                        @csrf
                        @include('genre/form' , ['genre' => $genre])
                        <button type="submit" class="pull-right btn btn-flat btn-primary mb-3">Edit</a>
                    </form>
                </div>
            </div>
        </div>
        <!-- Textual inputs end -->
    </div>
</div>
@endsection
