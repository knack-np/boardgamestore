@extends('app')
@section('title')
@endsection
@section('content')
<div class="main-content-inner">
    <div class="row">
        <!-- Dark table start -->
        <div class="col-12 mt-5">
            <div class="card">
                <div class="card-body">
                    <a href="game/create" type="button" class="pull-right btn btn-flat btn-dark mb-3">Add Game</a>
                    <div class="data-tables datatable-dark">
                        <table id="dataTable" class="text-center">
                            <thead class="text-capitalize">
                                <tr>
                                    <th class="sorting_disabled">Id</th>
                                    <th>Name</th>
                                    <th>Genre</th>
                                    <th>Player</th>
                                    <th>Age</th>
                                    <th>Time</th>
                                    <th>Price</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($games as $item)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$item->name_eng}}</td>
                                    <td>
                                        @forelse ($item->genres as $g)
                                            {{$g->genre->name_th}}
                                            @if(!$loop->last) , @endif
                                        @empty
                                            -
                                        @endforelse
                                    </td>
                                    <td>{{$item->min_player}}-{{$item->max_player}}</td>
                                    <td>{{$item->age_to_play}}+</td>
                                    <td>{{$item->time_to_play}} min</td>
                                    <td>{{$item->price}} ฿</td>
                                    <td><a href="game/{{$item->id}}/edit"> <i class="fa fa-edit"></i></a></td>
                                </tr>
                                @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- Dark table end -->

    </div>
</div>
@endsection
