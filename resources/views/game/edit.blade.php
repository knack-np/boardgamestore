@extends('app')
@section('content')
<div class="main-content-inner">
    <div class="row">
        <!-- Textual inputs start -->
        <div class="col-12 mt-5">
            <div class="card">
                <div class="card-body">
                    <form action="/game/{{$game->id}}" method="POST">
                        @method('PUT')
                        @csrf
                        @include('game/form' , ['game' => $game])
                        <button type="submit" class="pull-right btn btn-flat btn-dark mb-3">Edit</a>
                    </form>
                    <form action="/game/{{$game->id}}" method="POST">
                        @method('DELETE')
                        @csrf
                        <button type="button" class="pull-left btn btn-flat btn-danger mb-3" data-toggle="modal" data-target="#exampleModalLong" >Delete</button>
                        @include('game/modal-delete')
                    </form>
                </div>
            </div>
        </div>
        <!-- Textual inputs end -->
    </div>
</div>
@endsection
