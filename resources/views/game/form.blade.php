<div class="form-row">
    <div class="col-md-6 form-group">
        <label for="name_th" class="col-form-label">Name Thai</label>
        <input class="form-control" type="text" placeholder="test" name="name_th" @isset($game) value="{{$game->name_th}}" @endisset required>
    </div>
    <div class="col-md-6 form-group">
        <label for="name_eng" class="col-form-label">Name Eng</label>
        <input class="form-control" type="text" placeholder="test" name="name_eng" @isset($game) value="{{$game->name_eng}}" @endisset required>
    </div>
</div>
<div class="form-row">
    <div class="col-sm-3 mb-3 form-group">
        <label for="min_player" class="col-form-label">Min Player</label>
        <input class="form-control" type="number" placeholder="2" name="min_player" @isset($game) value="{{$game->min_player}}" @endisset required>
    </div>
    <div class="col-sm-3 mb-3 form-group">
        <label for="max_player" class="col-form-label">Max Player</label>
        <input class="form-control" type="number" placeholder="2" name="max_player" @isset($game) value="{{$game->max_player}}" @endisset required>
    </div>
    <div class="col-sm-3 mb-3 form-group">
        <label for="age_to_play" class="col-form-label">Age To Play</label>
        <input class="form-control" type="number" placeholder="2" name="age_to_play" @isset($game) value="{{$game->age_to_play}}" @endisset required>
    </div>
    <div class="col-sm-3 mb-3 form-group dropdown">
        <label class="col-form-label">Language</label>
        <select class="form-control" name="language" >
            @foreach ($languages as $item)
            <option value="{{$item->id}}"
                @isset($game)
                    @if ($game->language_id == $item->id)
                        selected
                    @endif
                @endisset>
                {{$item->name}}
            </option>
            @endforeach
        </select>
    </div>
</div>
<div class="form-row">
    <div class="col-sm-4 mb-3 form-group">
        <label for="complexity_level" class="col-form-label">Complexity Level</label>
        <input class="form-control" type="number" placeholder="1-10" name="complexity_level" @isset($game) value="{{$game->copmplexity_level}}" @endisset>
    </div>
    <div class="col-sm-4 mb-3 form-group">
        <label for="time_to_play" class="col-form-label">Time To Play </label>
        <input class="form-control" type="number" placeholder="2" name="time_to_play" @isset($game) value="{{$game->time_to_play}}" @endisset required>
    </div>
    <div class="col-sm-4 mb-3 form-group">
        <label for="price" class="col-form-label">Price</label>
        <input class="form-control" type="number" placeholder="3500" name="price" @isset($game) value="{{$game->price}}" @endisset required>
    </div>
</div>
<b class="text-muted mb-3 d-block">Genre</b>
@foreach ($genres as $item)
<div class="custom-control custom-checkbox custom-control-inline">
    <input type="checkbox" class="custom-control-input" id="{{$item->id}}"
    @isset($game)
        @foreach ($game->genres as $game_genre)
            @if ($game_genre->genre_id == $item->id)
                checked
            @endif
        @endforeach
    @endisset
    value="{{$item->id}}" name="genre[]" >
    <label class="custom-control-label" for="{{$item->id}}">{{$item->name_eng}}</label>
</div>
@endforeach

<div class="mt-3 form-group">
    <label for="description" class="col-form-label">Description</label>
    <input class="form-control" type="text" placeholder="test" name="description" @isset($game) value="{{$game->description_th}}" @endisset required>
</div>
<div class="form-row">
    <div class="col-sm-4 mb-3 form-group">
        <label for="author" class="col-form-label">Auther</label>
        <input class="form-control" type="text" placeholder="test" name="author" @isset($game) value="{{$game->author}}" @endisset>
    </div>
    <div class="col-sm-4 mb-3 form-group">
        <label for="designer" class="col-form-label">Designer</label>
        <input class="form-control" type="text" placeholder="test" name="designer" @isset($game) value="{{$game->designer}}" @endisset>
    </div>
    <div class="col-sm-4 mb-3 form-group">
        <label for="publisher" class="col-form-label">Publisher</label>
        <input class="form-control" type="text" placeholder="test" name="publisher" @isset($game) value="{{$game->publisher}}" @endisset>
    </div>
</div>
