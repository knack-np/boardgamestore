@extends('app')
@section('content')
<div class="main-content-inner">
    <div class="row">
        <!-- Textual inputs start -->
        <div class="col-12 mt-5">
            <div class="card">
                <div class="card-body">
                    <form action="/language" method="POST">
                        @csrf
                        @include('language/form')
                        <button type="submit" class="pull-right btn btn-flat btn-success mb-3">Create</button>
                    </form>
                </div>
            </div>
        </div>
        <!-- Textual inputs end -->
    </div>
</div>
@endsection
