@extends('app')
@section('content')
<div class="main-content-inner">
    <div class="row">
        <!-- Textual inputs start -->
        <div class="col-12 mt-5">
            <div class="card">
                <div class="card-body">
                    <form action="/language/{{$language->id}}" method="POST">
                        @method('PUT')
                        @csrf
                        @include('language/form' , ['language' => $language])
                        <button type="submit" class="pull-right btn btn-flat btn-success mb-3">Edit</a>
                    </form>
                </div>
            </div>
        </div>
        <!-- Textual inputs end -->
    </div>
</div>
@endsection
