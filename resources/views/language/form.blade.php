<div class="form-group">
    <label for="name" class="col-form-label">Name</label>
    <input class="form-control" type="text" id="name" name="name" @isset($language) value="{{$language->name}}" @endisset>
</div>
<div class="form-group">
    <label for="code" class="col-form-label">Code</label>
    <input class="form-control" type="search" id="code" name="code" @isset($language) value="{{$language->code}}" @endisset>
</div>
