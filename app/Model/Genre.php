<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Genre extends Model
{
    protected $table = 'genres';
    protected $guarded = ['id'];
    protected $dates = ['deleted_at'];
}
