<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class GameGenre extends Model
{
    protected $table = 'game_genres';
    protected $guarded = ['id'];

    public function genre()
    {
        return $this->hasOne('App\Model\Genre','id','genre_id');
    }
}
