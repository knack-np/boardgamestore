<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Game extends Model
{

    protected $table = 'games';
    protected $guarded = ['id'];

    public function genres()
    {
        return $this->hasMany('App\Model\GameGenre');
    }
    public function photos()
    {
        return $this->hasMany('App\Model\GamePhotos');
    }
    public function language()
    {
        return $this->hasOne('App\Model\Language');
    }
}
