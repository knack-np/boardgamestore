<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class GamePhoto extends Model
{
    protected $table = 'GamePhoto';
    protected $guarded = ['id'];
}
