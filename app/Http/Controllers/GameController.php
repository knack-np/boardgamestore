<?php

namespace App\Http\Controllers;

use App\Model\Game;
use App\Model\GameGenre;
use App\Model\Genre;
use App\Model\Language;
use Illuminate\Http\Request;

class GameController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $games = Game::orderBy('id', 'asc')->get();

        // dd($games);

        return view('game/index')
        ->with('page_name','Game')
        ->with('games',$games);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $genres = Genre::orderBy('id', 'asc')->get();
        $languages = Language::orderBy('id', 'asc')->get();
        return view('game/create')
        ->with('page_name','Game')
        ->with('action','create')
        ->with('genres',$genres)
        ->with('languages',$languages);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $game = new Game;
        $game->name_th = $request->name_th;
        $game->name_eng = $request->name_eng;
        $game->min_player = $request->min_player;
        $game->max_player = $request->max_player;
        $game->age_to_play = $request->age_to_play;
        $game->time_to_play = $request->time_to_play;
        $game->language_id = $request->language;
        $game->complexity_level = $request->complexity_level;
        $game->price = $request->price;
        $game->description_th = $request->description;
        $game->author = $request->author;
        $game->designer = $request->designer;
        $game->publisher = $request->publisher;
        $game->save();

        if ($request->genre) {
            foreach ($request->genre as $key => $value) {
                $game_genre = new GameGenre;
                $game_genre->game_id = $game->id;
                $game_genre->genre_id = $value;
                $game_genre->save();
            }
        }


        return redirect('game');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Game  $game
     * @return \Illuminate\Http\Response
     */
    public function show(Game $game)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Game  $game
     * @return \Illuminate\Http\Response
     */
    public function edit(Game $game)
    {
        $genres = Genre::orderBy('id', 'asc')->get();
        $languages = Language::orderBy('id', 'asc')->get();
        return view('game/edit')
        ->with('page_name', 'Game')
        ->with('action', 'edit')
        ->with('game',$game)
        ->with('genres',$genres)
        ->with('languages',$languages);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Game  $game
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Game $game)
    {
        $game->name_th = $request->name_th;
        $game->name_eng = $request->name_eng;
        $game->min_player = $request->min_player;
        $game->max_player = $request->max_player;
        $game->age_to_play = $request->age_to_play;
        $game->time_to_play = $request->time_to_play;
        $game->complexity_level = $request->complexity_level;
        $game->price = $request->price;
        $game->description_th = $request->description;
        $game->author = $request->author;
        $game->designer = $request->designer;
        $game->publisher = $request->publisher;
        $game->save();



        if ($request->genre) {
            $deletedRows = GameGenre::where('game_id', $game->id)->delete();
            foreach ($request->genre as $key => $value) {
                $game_genre = new GameGenre;
                $game_genre->game_id = $game->id;
                $game_genre->genre_id = $value;
                $game_genre->save();
            }
        }

        return redirect('game');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Game  $game
     * @return \Illuminate\Http\Response
     */
    public function destroy(Game $game)
    {
        $deletedGenres = GameGenre::where('game_id', $game->id)->delete();

        $deletedGame = Game::where('id', $game->id)->delete();

        return redirect('game');
    }
}
