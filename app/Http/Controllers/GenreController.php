<?php

namespace App\Http\Controllers;

use App\Model\Genre;
use Illuminate\Http\Request;

class GenreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $genres = Genre::orderBy('id', 'asc')->get();
        return view('genre/index')
        ->with('page_name','Genre')
        ->with('genres',$genres);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('genre/create')
        ->with('page_name','Genre')
        ->with('action','create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $genre = new Genre;
        $genre->name_th = $request->name_th;
        $genre->name_eng = $request->name_eng;
        $genre->save();

        return redirect('genre');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Genre  $genre
     * @return \Illuminate\Http\Response
     */
    public function show(Genre $genre)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Genre  $genre
     * @return \Illuminate\Http\Response
     */
    public function edit(Genre $genre)
    {
        return view('genre/edit')
        ->with('page_name', 'Genre')
        ->with('action', 'edit')
        ->with('genre',$genre);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Genre  $genre
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Genre $genre)
    {
        $genre->name_th = $request->name_th;
        $genre->name_eng = $request->name_eng;
        $genre->save();

        return redirect('genre');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Genre  $genre
     * @return \Illuminate\Http\Response
     */
    public function destroy(Genre $genre)
    {
        // $genre->delete();

        // return view('/genre/index')
        // ->with('page_name','Genre');
    }
}
